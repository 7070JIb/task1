﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CubeMover : MonoBehaviour
{
    public FixedJoystick joystick;
    public NavMeshAgent agent;
    public GameObject arrowPrefab;
    public GameObject activeArrow;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (joystick.Direction.magnitude > 0.1f)
        {
            agent.isStopped = true;
            if (activeArrow != null) Destroy(activeArrow);
            Vector2 playerSpeed = joystick.Direction.normalized * Time.deltaTime * agent.speed;
            agent.Move(new Vector3(-playerSpeed.y, 0, playerSpeed.x) );
        }
        else
        {
            if (CheckPointing())
            {
                agent.isStopped = false;
            }
            UpdateArrow();
        }
    }

    private void UpdateArrow()
    {
        if(activeArrow!= null && (activeArrow.transform.position - agent.transform.position).magnitude < 2f)
        {
            Destroy(activeArrow);
        }
    }

    private bool CheckPointing()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hitFromScreen, 100f))
            {
                agent.destination = hitFromScreen.point;
                if (activeArrow != null) Destroy(activeArrow);
                activeArrow = Instantiate(arrowPrefab);
                Physics.Raycast(new Ray(new Vector3(hitFromScreen.point.x, 10f, hitFromScreen.point.z), Vector3.down), out RaycastHit hitFromAbove, 100f);

                activeArrow.transform.position = hitFromAbove.point;
                Debug.Log(hitFromScreen.point);
                return true;
            }
        }
        return false;
    }
}
